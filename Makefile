.PHONY: build

run:
	cargo run

build:
	cargo build

clean:
	cargo clean
